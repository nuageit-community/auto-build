<div align="center">

<p>
  <img alt="gif-header" src="https://cdn.hackernoon.com/hn-images/0*KyeIBTwEiX6_sE06" width="350px" float="center"/>
</p>

<h2 align="center">✨ Auto build image ✨</h2>

<div align="center">

[![Semantic Release](https://img.shields.io/badge/%20%20%F0%9F%93%A6%F0%9F%9A%80-semantic--release-e10079.svg)](https://gitlab.com/dry-group/cluster-management)
[![Commitizen friendly](https://img.shields.io/badge/commitizen-friendly-brightgreen.svg)](https://gitlab.com/dry-group/cluster-management)

</div>

---

<p align="center">
  <img alt="gif-about" src="https://thumbs.gfycat.com/GrizzledRemoteHornet-small.gif" width="450px" float="center"/>
</p>

<p align="center">
  ✨ A automation used in [Auto DevOps](https://gitlab.com/nuageit/shared/auto-devops) pipeline context to build new docker images and push to GitLab container registry ✨
</p>

<p align="center">
  <a href="#getting-started">Getting Started</a>&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;
  <a href="#contributing">Contributing</a>&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;
  <a href="#versioning">Versioning</a>
</p>

</div>

---

## ➤ Getting Started <a name = "getting-started"></a>

If you want contribute on this project, first you need to make a **git clone**:

```bash
git clone --depth 1 <https://gitlab.com/nuageit/shared/auto-build.git> -b main
```

This will give you access to the code on your **local machine**.

## ➤ Description <a name = "description"></a>

Mostly it is used during CI (continuous integration) as part of an automated build.

### GitLab CI

Variables used in `.gitlab-ci.yml`:

- `AUTO_DEVOPS_BUILD_REPOSITORY`
- `AUTO_DEVOPS_BUILD_IMAGE_TAG`
- `AUTO_DEVOPS_DOCKERFILE_PATH`
- `AUTO_DEVOPS_HARBOR_SERVER`
- `AUTO_DEVOPS_HARBOR_USER`
- `AUTO_DEVOPS_HARBOR_PASSWORD`
- `AUTO_DEVOPS_BUILD_EXTRA_IMAGE_TAGS`
- `AUTO_DEVOPS_BUILD_ARGS`
- `GITLAB_DEPENDENCY_PROXY_SERVER`
- `GITLAB_DEPENDENCY_PROXY_USER`
- `GITLAB_DEPENDENCY_PROXY_PASSWORD`

### GitLab Container Registry

| Git Branch | Git Tag    | Docker Tag                                                                                                 | Kubernetes Environment |
|------------|------------|------------------------------------------------------------------------------------------------------------|------------------------|
| feature    | none       | registry.gitlab.com/nuageit/pocs/docker-build-flow/feature-dlv-12:31b74aa78fbc54a68eb35a1ba372fdb1a1b49336 | develop                |
| feature    | none       | registry.gitlab.com/nuageit/pocs/docker-build-flow/feature-dlv-12:latest                                   | develop                |
| develop    | none       | registry.gitlab.com/nuageit/pocs/docker-build-flow/develop:577eb32c40e6dce0ba123bc96acf7409e51ed220        | develop                |
| develop    | none       | registry.gitlab.com/nuageit/pocs/docker-build-flow/develop:latest                                          | develop                |
| release    | 1.0.0-rc.1 | registry.gitlab.com/nuageit/pocs/docker-build-flow:1.0.0-rc.1                                              | none                   |
| release    | 1.0.0-rc.1 | registry.gitlab.com/nuageit/pocs/docker-build-flow:rc                                                      | none                   |
| sandbox    | none       | registry.gitlab.com/nuageit/pocs/docker-build-flow:1.0.0-rc.0                                              | sandbox                |
| sandbox    | none       | registry.gitlab.com/nuageit/pocs/docker-build-flow:rc                                                      | sandbox                |
| stage      | none       | registry.gitlab.com/nuageit/pocs/docker-build-flow:1.0.0-rc.0                                              | stage                  |
| stage      | none       | registry.gitlab.com/nuageit/pocs/docker-build-flow:rc                                                      | stage                  |
| main       | 1.0.0      | registry.gitlab.com/nuageit/pocs/docker-build-flow:1.0.0                                                   | production             |
| main       | 1.0.0      | registry.gitlab.com/nuageit/pocs/docker-build-flow:stable                                                  | production             |
| main       | 1.0.0      | registry.gitlab.com/nuageit/pocs/docker-build-flow:latest                                                  | production             |

## ➤ Links <a name = "links"></a>

- <https://gitlab.com/gitlab-org/gitlab-runner/-/issues/27300>
- <https://gitlab.com/nuageit/pocs/docker-build-flow>
- <https://github.com/jdrouet/docker-with-buildx>
- <https://tldp.org/LDP/Bash-Beginners-Guide/html/sect_07_01.html>
- <https://itnext.io/multi-arch-docker-images-in-gitlab-container-registry-ada46cc6d4bb>
- <https://forum.gitlab.com/t/build-multi-arch-docker-image/23569>
- <https://stackoverflow.com/questions/61430005/gitlab-ci-cd-building-multiarch-docker-images>
- <https://www.nithinbose.com/posts/build-multi-arch-docker-images-on-gitlab/>
- <https://stackoverflow.com/questions/48009595/how-do-i-enable-dockers-experimental-features-so-gitlab-can-build-with-it>
- <https://stackoverflow.com/questions/64749850/how-to-build-arm-64-docker-image-with-yaml-in-aws-for-graviton2-processors>

## ➤ Visuals <a name = "visuals"></a>

Depending on what you are making, it can be a good idea to include screenshots or even a video (you'll frequently see GIFs rather than actual videos). Tools like ttygif can help, but check out Asciinema for a more sophisticated method.

## ➤ Usage <a name = "usage"></a>

Use examples liberally, and show the expected output if you can. It's helpful to have inline the smallest example of usage that you can demonstrate, while providing links to more sophisticated examples if they are too long to reasonably include in the README.

## ➤ Versioning <a name = "versioning"></a>

To check the change history, please access the [**CHANGELOG.md**](CHANGELOG.md) file.

## ➤ Contributing <a name = "contributing"></a>

Contributions, issues and feature requests are welcome. Feel free to check issues page if you want to contribute. [Check the contributing guide](https://nuageit.atlassian.net/wiki/spaces/OPSNUAGE/pages/1995309068/Processo+de+contribui+o).

## ➤ Troubleshooting <a name = "troubleshooting"></a>

If you have any problems, please contact **DevOps Team**.

## ➤ Project status <a name = "project-status"></a>

If you have run out of energy or time for your project, put a note at the top of the README saying that development has slowed down or stopped completely. Someone may choose to fork your project or volunteer to step in as a maintainer or owner, allowing your project to keep going. You can also make an explicit request for maintainers. Currently the project is constantly being updated! 👾

## ➤ Show your support <a name = "show-your-support"></a>

<div align="center">

Give me a ⭐️ if this project helped you!

<p>
  <img alt="gif-header" src="https://www.icegif.com/wp-content/uploads/baby-yoda-bye-bye-icegif.gif" width="350px" float="center"/>
</p>

Made with 💜 by **DevOps Team** :wave: inspired on [readme-md-generator](https://github.com/kefranabg/readme-md-generator)

</div>
