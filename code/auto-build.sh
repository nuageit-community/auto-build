#!/bin/bash -e

# This option will make the script exit when there is an error
set -o errexit

# This option ensures that your script exits as soon as it encounters any failed piped commands
set -o pipefail

# This option will trace what gets executed. Useful for debugging
[[ "$TRACE" ]] && set -o xtrace

# ==============================================================================
# COLORS VARIABLES
# ==============================================================================

# High Intensity
GREEN="\\033[0;92m"  # Green
YELLOW="\\033[0;93m" # Yellow
PURPLE="\\033[0;95m" # Purple
CYAN="\\033[0;96m"   # Cyan
NC="\\033[0;97m"     # White

# ==============================================================================
# COMMON VARIABLES
# ==============================================================================

VERSION="1.10.2-SNAPSHOT"

# shellcheck disable=SC2155
readonly PROGNAME=$(basename "$0")

# Font Name: ANSI Shadow
HEADER="



 █████╗ ██╗   ██╗████████╗ ██████╗     ██████╗ ██╗   ██╗██╗██╗     ██████╗
██╔══██╗██║   ██║╚══██╔══╝██╔═══██╗    ██╔══██╗██║   ██║██║██║     ██╔══██╗
███████║██║   ██║   ██║   ██║   ██║    ██████╔╝██║   ██║██║██║     ██║  ██║
██╔══██║██║   ██║   ██║   ██║   ██║    ██╔══██╗██║   ██║██║██║     ██║  ██║
██║  ██║╚██████╔╝   ██║   ╚██████╔╝    ██████╔╝╚██████╔╝██║███████╗██████╔╝
╚═╝  ╚═╝ ╚═════╝    ╚═╝    ╚═════╝     ╚═════╝  ╚═════╝ ╚═╝╚══════╝╚═════╝



"

USAGE="Hello there, fellow coders 🤖!

This is the ${CYAN}Auto Build${NC} script powerd by ${YELLOW}DevOps Team${NC}

${PROGNAME} [-h] [-v] [-e]

${PURPLE}Flags${NC}:
    -e, --exec              Exec auto build script
    -h, --help              Show this help text
    -v, --version           Prints the kubetail version

${GREEN}Examples${NC}:
    ${PROGNAME} --exec
    ${PROGNAME} --help
    ${PROGNAME} --version"

# ==============================================================================
# AUTO DEVOPS BUILD VARIABLES
# ==============================================================================

AUTO_DEVOPS_IMAGE_LABELS="--label org.opencontainers.image.vendor=$CI_SERVER_URL/$GITLAB_USER_LOGIN
  --label org.opencontainers.image.authors=$CI_SERVER_URL/$GITLAB_USER_LOGIN
  --label org.opencontainers.image.revision=$CI_COMMIT_SHA
  --label org.opencontainers.image.source=$CI_PROJECT_URL
  --label org.opencontainers.image.documentation=$CI_PROJECT_URL
  --label org.opencontainers.image.licenses=$CI_PROJECT_URL
  --label org.opencontainers.image.url=$CI_PROJECT_URL
  --label vcs-url=$CI_PROJECT_URL
  --label com.gitlab.ci.user=$CI_SERVER_URL/$GITLAB_USER_LOGIN
  --label com.gitlab.ci.email=$GITLAB_USER_EMAIL
  --label com.gitlab.ci.tagorbranch=$CI_COMMIT_REF_NAME
  --label com.gitlab.ci.pipelineurl=$CI_PIPELINE_URL
  --label com.gitlab.ci.commiturl=$CI_PROJECT_URL/commit/$CI_COMMIT_SHA
  --label com.gitlab.ci.cijoburl=$CI_JOB_URL
  --label com.gitlab.ci.mrurl=$CI_PROJECT_URL/-/merge_requests/$CI_MERGE_REQUEST_ID"

image_previous="$AUTO_DEVOPS_BUILD_REPOSITORY:$CI_COMMIT_BEFORE_SHA"
image_tagged="$AUTO_DEVOPS_BUILD_REPOSITORY:$AUTO_DEVOPS_BUILD_IMAGE_TAG"
image_latest="$AUTO_DEVOPS_BUILD_REPOSITORY:latest"

# ==============================================================================
# COMMON FUNCTIONS
# ==============================================================================

Status() {
  echo -e "${PURPLE}[INFO]${NC}: $1"
}

Abort() {
  echo >&2 '
  ************************
  ***  ❌ ABORTED ❌  ***
  ************************
  '
  echo "An error has occurred - $1. Aborting..." >&2 && exit 1
}

GitLab_Load_Envs() {
  CI_COMMIT_SHA="${CI_COMMIT_SHA:-none}"
  CI_COMMIT_BEFORE_SHA="${CI_COMMIT_BEFORE_SHA:-none}"
  CI_COMMIT_BRANCH="${CI_COMMIT_BRANCH:-none}"
  CI_COMMIT_REF_SLUG="${CI_COMMIT_REF_SLUG:-none}"
  CI_COMMIT_TAG="${CI_COMMIT_TAG:-none}"
  CI_COMMIT_TITLE="${CI_COMMIT_TITLE:-none}"
  CI_COMMIT_TIMESTAMP="${CI_COMMIT_TIMESTAMP:-none}"
  CI_COMMIT_REF_PROTECTED="${CI_COMMIT_REF_PROTECTED:-none}"
}

Welcome() {
  OS="$(uname)"
  [ "$OS" = "Linux" ] && DATE_CMD="date" || DATE_CMD="gdate"
  DATE_INFO=$($DATE_CMD +"%Y-%m-%d %T")
  DATE_INFO_SHORT=$($DATE_CMD +"%A %B")
  GitLab_Load_Envs && Status "
  ╔═══════════════════════════════════════════════════════════════════════
  ║
  ║             ✨ Welcome $USER! ✨
  ║
  ║ 🔖 Date    - It's now $DATE_INFO - $DATE_INFO_SHORT
  ║ 🔖 System  - $OS CI context
  ║
  ║                 👾 Commit Information 👾
  ║
  ║ 🔖 Commit SHA        - $CI_COMMIT_SHA
  ║ 🔖 Commit before SHA - $CI_COMMIT_BEFORE_SHA
  ║ 🔖 Commit branch     - $CI_COMMIT_BRANCH
  ║ 🔖 Commit ref        - $CI_COMMIT_REF_SLUG
  ║ 🔖 Commit tag        - $CI_COMMIT_TAG
  ║ 🔖 Commit title      - $CI_COMMIT_TITLE
  ║ 🔖 Commit timestamp  - $CI_COMMIT_TIMESTAMP
  ║ 🔖 Commit protected  - $CI_COMMIT_REF_PROTECTED
  ║
  ╚═══════════════════════════════════════════════════════════════════════"

  Status "👾 Show auto devops image labels: $AUTO_DEVOPS_IMAGE_LABELS"
  Status "👾 Show image previous: $image_previous"
  Status "👾 Show image tagged: $image_tagged"
  Status "👾 Show image latest: $image_latest"
}

# ==============================================================================
# FUNCTIONS
# ==============================================================================

Check_Docker_Host() {
  if ! docker info &>/dev/null; then
    if [ -z "$DOCKER_HOST" ] && [ "$KUBERNETES_PORT" ]; then
      Status "🔖 Setup default value to DOCKER_HOST"
      export DOCKER_HOST="tcp://localhost:2375"
    fi
  fi
}

Check_Dockerfile() {
  if [[ -n "${AUTO_DEVOPS_DOCKERFILE_PATH}" ]]; then
    Status "⚙️ Building Dockerfile-based application using '$AUTO_DEVOPS_DOCKERFILE_PATH' path"
  else
    export AUTO_DEVOPS_DOCKERFILE_PATH="Dockerfile"
  fi

  if [[ ! -f "${AUTO_DEVOPS_DOCKERFILE_PATH}" ]]; then
    Status "⚠️ Unable to find '$AUTO_DEVOPS_DOCKERFILE_PATH'. Exiting..." >&2
    exit 1
  fi
}

Login_GitLab_Registry() {
  Status "🎊 Logging in GitLab Container Registry with CI credentials..."
  echo "$CI_REGISTRY_PASSWORD" | docker login -u "$CI_REGISTRY_USER" --password-stdin "$CI_REGISTRY"
}

Login_GitLab_Dependency_Proxy() {
  Status "🎊 Logging in GitLab Dependency Proxy with CI credentials..."
  echo "$GITLAB_DEPENDENCY_PROXY_PASSWORD" | docker login -u "$GITLAB_DEPENDENCY_PROXY_USER" --password-stdin "$GITLAB_DEPENDENCY_PROXY_SERVER"
}

Pull_Docker_Images() {
  Status "🔖 Attempting to pull a previously built image for use with --cache-from..."
  docker image pull --quiet "$image_previous" ||
    docker image pull --quiet "$image_latest" ||
    echo "No previously cached image found. The docker build will proceed without using a cached image"
}

Prepare_Docker_Build() {
  Check_Docker_Host &&
    Check_Dockerfile &&
    Login_GitLab_Registry &&
    Login_GitLab_Dependency_Proxy

  # By default we support DOCKER_BUILDKIT, however it can be turned off
  # by explicitly setting this to an empty string
  DOCKER_BUILDKIT=${DOCKER_BUILDKIT:-1}

  extra_tags=()
  if [[ -n "$AUTO_DEVOPS_BUILD_EXTRA_IMAGE_TAGS" ]]; then
    Status "👾 Show auto devops build extra image tags: $AUTO_DEVOPS_BUILD_EXTRA_IMAGE_TAGS"
    # shellcheck disable=SC1117
    mapfile -t extra_tag_names < <(echo "$AUTO_DEVOPS_BUILD_EXTRA_IMAGE_TAGS" | tr ',' "\n")
    for extra_tag_name in "${extra_tag_names[@]}"; do
      extra_tags+=('--tag' "$extra_tag_name")
    done
  fi

  build_args=(
    -f "$AUTO_DEVOPS_DOCKERFILE_PATH"
    --tag "$image_tagged"
    --tag "$image_latest"
    "${extra_tags[@]}"
  )
  Status "👾 Show auto devops build args: ${build_args[*]}"
}

Docker_Buildx() {
  Prepare_Docker_Build

  # The docker-container driver is required for this cache type
  docker run --rm --privileged multiarch/qemu-user-static --reset -p yes; docker buildx create --use

  Status "👾 Show docker info"
  docker info

  Status "👾 Show buildx inspect"
  docker buildx inspect --bootstrap

  Status "👾 Show auto devops build mult arch plataforms: $AUTO_DEVOPS_BUILD_MULT_ARCH_PLATAFORMS"

  Status "🛠 Start docker buildx"
  docker buildx build \
    "${build_args[@]}" \
    --platform $AUTO_DEVOPS_BUILD_MULT_ARCH_PLATAFORMS \
    --progress=plain $AUTO_DEVOPS_BUILD_EXTRA_ARGS $AUTO_DEVOPS_IMAGE_LABELS \
    --push \
    . 2>&1

  Status "👾 Show docker manifest image tagged"
  docker manifest inspect $image_tagged
}

Docker_Build() {
  Prepare_Docker_Build

  Status "🛠 Start docker build"
  # shellcheck disable=SC2086
  docker build \
    "${build_args[@]}" \
    --progress=plain $AUTO_DEVOPS_BUILD_EXTRA_ARGS $AUTO_DEVOPS_IMAGE_LABELS \
    . 2>&1

  docker push "$image_tagged"
  docker push "$image_latest"

  if [[ -n "$AUTO_DEVOPS_BUILD_EXTRA_IMAGE_TAGS" ]]; then
    # shellcheck disable=SC1117
    mapfile -t extra_tag_names < <(echo "$AUTO_DEVOPS_BUILD_EXTRA_IMAGE_TAGS" | tr ',' "\n")
    for extra_tag_name in "${extra_tag_names[@]}"; do
      docker push "$extra_tag_name"
    done
  fi
}

# ==============================================================================
# CALL FUNCTIONS
# ==============================================================================

Welcome && echo "$HEADER"

if [ "$CI" ]; then
  Status "🎉 We are in a CI context 🎉"
else
  Status "🚨 We are not in a CI context..." && Abort "CI exception"
fi

if [ "$#" -ne 0 ]; then
  while [ "$#" -gt 0 ]; do
    case "$1" in
    -e | --exec)
      if [[ "${AUTO_DEVOPS_BUILD_MULT_ARCH}" == "true" ]]; then
        Docker_Buildx
      else
        Docker_Build
      fi
      exit 0
      ;;
    -h | --help)
      echo "$USAGE"
      exit 0
      ;;
    -v | --version)
      echo -e "${PURPLE}Version${NC}: $VERSION"
      exit 0
      ;;
    --)
      break
      ;;
    -*)
      echo "Invalid option '$1'. Use --help to see the valid options" >&2
      exit 1
      ;;
    # an option argument, continue
    *) ;;
    esac
    shift
  done
else
  echo -e "$USAGE"
  exit 1
fi
