ARG BUILDX_VERSION=0.6.3
ARG DOCKER_VERSION=20.10.9-alpine3.14

FROM alpine:3.13.6 AS fetcher
RUN apk add --no-cache curl
ARG BUILDX_VERSION
RUN curl -L \
  --output /docker-buildx \
  "https://github.com/docker/buildx/releases/download/v${BUILDX_VERSION}/buildx-v${BUILDX_VERSION}.linux-amd64" \
  && chmod a+x /docker-buildx

FROM docker:${DOCKER_VERSION}
RUN apk add --no-cache bash
COPY --from=fetcher [ "/docker-buildx", "/usr/local/libexec/docker/cli-plugins/docker-buildx" ]
COPY  [ "./code", "/root/" ]
RUN chmod +x /root/auto-build.sh
CMD [ "/root/auto-build.sh" ]
